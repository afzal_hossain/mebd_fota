#include <cstring>
#include <string.h>
#include <stdlib.h>
#include <Utils/BufferUtils.h>




BufferUtils::BufferUtils(int len){

	cBuffer = aBuffer ;
	bufferSize = BUFFER_SIZE ;
}
BufferUtils::~BufferUtils(){

}

void BufferUtils::reset(){

	char * buffToClear = aBuffer;
	for(int i=0; i<bufferSize; i++){
		*buffToClear++ = '\0';
	}
	cBuffer = aBuffer ;
}
bool BufferUtils::hasNext(){

	if(*cBuffer) return true;
	else   		 return false;

}
bool BufferUtils::isBufferEmpty(){


}
bool BufferUtils::isBufferFull(){


}

int BufferUtils::numberOfElementsInBuffer(){

	return strlen(aBuffer);

}
int BufferUtils::freeSpaceInBuffer(){


}

const char *  BufferUtils::get(){

	return aBuffer;

}
const char BufferUtils::getC(){

	return *cBuffer++;

}

void BufferUtils::put(const char * buf){

	strcpy(aBuffer,buf);
	cBuffer = aBuffer;

}
void BufferUtils::putC(const char c){

	*cBuffer++ = c;

}
