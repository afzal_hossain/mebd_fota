#pragma once

#include "mbed.h"
#include <Utils/BufferUtils.h>


class SerialUtils : public SerialBase{

public:
	SerialUtils(PinName tx, PinName rx, int baudrate, int rxBuffer, int txBuffer);
    virtual ~SerialUtils(void);

//    int writeable(void);
//    int putc(int c);
//    int put(const void* buffer, int length, bool blocking);
//    int readable(void);
//    int getc(void);
//    int get(void* buffer, int length, bool blocking);
    void sendStringToSerial(const char* buffer);
    void sendByteToSerial(const char byte);

protected:
    //! receive interrupt routine
    void rxIrqBuf(void);
    //! transmit interrupt woutine
    void txIrqBuf(void);
    //! start transmission helper
    void txFirsChar(void);
    //! move bytes to hardware
    void txToSerial(void);

    BufferUtils gsmRxBuffer;
    BufferUtils gsmTxBuffer;
};
