#pragma once

#define BUFFER_SIZE 350


class BufferUtils{

public:
	BufferUtils(int len);
	~BufferUtils();

	void reset();
	bool hasNext();
	bool isBufferEmpty();
	bool isBufferFull();

	int numberOfElementsInBuffer();
	int freeSpaceInBuffer();

	const char * get();
	const char getC();

	void put(const char * buf);
	void putC(const char c);


private:
	char aBuffer[BUFFER_SIZE];
	char * cBuffer;
	int bufferSize;
};


