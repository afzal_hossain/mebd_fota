#pragma once

#include "mbed.h"


#define DEBUX_RX 		PA_3
#define DEBUG_TX 		PA_2
#define DEBUG_BAUDRATE  9600U

class Mdebug : public SerialBase{

public:
	Mdebug(void);
	~Mdebug(void);

	void print_log(const char * log);
	void print_log_with_nul(const char *log,int len);
};
