#include <Utils/Mdebug.h>


Mdebug::Mdebug(void):SerialBase(DEBUG_TX,DEBUX_RX,DEBUG_BAUDRATE)
{

}

Mdebug::~Mdebug(void){

}
void Mdebug::print_log(const char * log){
	while(*log){
		SerialBase::_base_putc(*log++);
	}
}
void Mdebug::print_log_with_nul(const char * log,int len){
	while(len--){
		SerialBase::_base_putc(*log++);
	}
}
