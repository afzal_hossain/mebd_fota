#include <Utils/SerialUtils.h>

SerialUtils::SerialUtils(PinName tx, PinName rx, int baudrate, int rxBuffer, int txBuffer) :
SerialBase(tx,rx,baudrate),
			 gsmRxBuffer(rxBuffer),
			 gsmTxBuffer(txBuffer)
{
    if (rx!=NC)
        attach(this, &SerialUtils::rxIrqBuf, RxIrq);
}

SerialUtils::~SerialUtils(void)
{
    attach(NULL, RxIrq);
    attach(NULL, TxIrq);
}

void SerialUtils::sendStringToSerial(const char* buffer){

	gsmTxBuffer.reset();
	gsmRxBuffer.reset();
	gsmTxBuffer.put(buffer);
	txFirsChar();
}
void SerialUtils::txFirsChar(){

	attach(NULL, TxIrq); // disable the tx isr to avoid interruption
	txToSerial();
	if (gsmTxBuffer.hasNext()){

		attach(this, &SerialUtils::txIrqBuf, TxIrq); //activate txInterrupt to send next data.....
	}
}
void SerialUtils::txToSerial(void){

	while (!SerialUtils::writeable());
	SerialUtils::_base_putc(gsmTxBuffer.getC());

}
void SerialUtils::txIrqBuf(void){
	txToSerial();
    if (!gsmTxBuffer.hasNext())
        attach(NULL, TxIrq);
}

void SerialUtils::sendByteToSerial(const char byte){


}

void SerialUtils::rxIrqBuf(void){

    while (!SerialUtils::readable());
    gsmRxBuffer.putC(SerialUtils::_base_getc());

}

