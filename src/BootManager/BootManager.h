#pragma once

#include "BootLoader.h"
#include "Ftp.h"

class BootManager : public BootLoader, public Ftp{

public:
	BootManager(GsmParser &parser);
	~BootManager(void);
	bool downAndSaveFirmwareToGsmModuleFlash(void);
private:
	bool UpgradeDeviceFirmware();
	GsmParser *gParser;

};
