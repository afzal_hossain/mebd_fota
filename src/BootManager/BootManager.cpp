#include "BootManager.h"
#include <stdlib.h>


#define debug_print
#ifdef debug_print
#include "Utils/Mdebug.h"
Mdebug ebug;
#endif


BootManager::BootManager(GsmParser &parser ):gParser(&parser),Ftp(parser){


}
bool BootManager::downAndSaveFirmwareToGsmModuleFlash(void){

	//bool taskSttus = downloadAndSaveFileToGsmModuleFlash("file name");

	executeFlashErase(3 , 5);

	UpgradeDeviceFirmware();
	return true;

}
bool BootManager::UpgradeDeviceFirmware(){
	gParser->checkIfFileExistsInGsmModuleFlash("no file path yet");
	int fileSize =  gParser->getFileSize("baal","shaua");

#ifdef debug_print
	char buff[50];
	itoa (fileSize,buff,10);
	ebug.print_log("BootManager::UpgradeDeviceFirmware::fileSize:: ");
	ebug.print_log(buff);
#endif

	const char * fPacket;
	int packetSize = 256;
	char dataPacket[packetSize];
	int position = 0;
    int numberOfPacketToDownload = (fileSize/packetSize) ;

    for(int i=0; i <= numberOfPacketToDownload; i++){

    	for(int ct=0; ct<packetSize; ct++){
    		dataPacket[i] = '\0';
    	}

    	position = i*packetSize;
    	if((fileSize-position) < packetSize ){
    		packetSize =  fileSize-position;
    	}
    	fPacket = gParser->downloadFilePacketFromGsmModuleFlash("bld_app_no_mbed.bin",packetSize,position);
    	for(;;){

    		*fPacket++;
    		if(*fPacket == '\r' || *fPacket == '\n'){
    			continue;
    		}else{
    			for(int i=0;i<packetSize;i++){
    				dataPacket[i] = *fPacket++;
    			}
    			break;
    		}
    	}


#ifdef debug_print
    	char buff[50];
    	itoa (fileSize,buff,10);
    	ebug.print_log(":::executing flash program... \n ");
#endif

    	executeFlashUpdate(dataPacket,MAIN_APPS_BASE_ADDRESS+position, packetSize);
    }
    gotoApplication();

}
