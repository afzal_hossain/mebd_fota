#pragma once


#include "mbed.h"
#include <stdint.h>

//#define POST_APPLICATION_ADDR 0x08008000U

#define FLASH_SECTOR_ADDRESS_SECTOR_0	0x08000000U
#define FLASH_SECTOR_ADDRESS_SECTOR_1	0x08004000U
#define FLASH_SECTOR_ADDRESS_SECTOR_2	0x08008000U
#define FLASH_SECTOR_ADDRESS_SECTOR_3	0x0800C000U
#define FLASH_SECTOR_ADDRESS_SECTOR_4   0x08010000U
#define FLASH_SECTOR_ADDRESS_SECTOR_5	0x08020000U
#define FLASH_SECTOR_ADDRESS_SECTOR_6	0x08040000U
#define FLASH_SECTOR_ADDRESS_SECTOR_7	0x08060000U


#define MAIN_APPS_BASE_ADDRESS 0x08008000U






class BootLoader{

public:
	BootLoader(void);
	~BootLoader(void);

	void executeFlashErase(uint8_t fromSector , uint8_t numberOfSector);
	void executeFlashUpdate(char *dataBuffer, uint32_t address,uint16_t pageSize);
	void gotoApplication();

private:
	FlashIAP flash;

};
