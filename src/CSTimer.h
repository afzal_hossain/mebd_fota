#pragma once

#include "Mbed.h"


class CSTimer {

public:
	CSTimer(void);
	~CSTimer(void);

	Ticker ticker;
	static int myCstimerTimeOut;

	void initMyCstimerFor100ms();
private:
	void myCstimer();
};
