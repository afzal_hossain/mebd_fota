#include "mbed.h"
#include "GsmModem.h"
#include "CSTimer.h"
#include "BootManager.h"




int main(){
	
	CSTimer csTimer; // initialize CSTimer to get interrupt 100ms interval
	csTimer.initMyCstimerFor100ms();

	GsmModem *gsmModem = GsmModem::getInstance();
	gsmModem ->init();

	BootManager bootManager(*gsmModem);
	bootManager.downAndSaveFirmwareToGsmModuleFlash();

	while(1);



}
