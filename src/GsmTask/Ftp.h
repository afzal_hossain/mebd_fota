#include "mbed.h"
#include "GsmParser.h"

class Ftp{

public:

	Ftp(GsmParser &parser);
	~Ftp();
	bool downloadAndSaveFileToGsmModuleFlash(const char * fileName);
private:

	bool init();
	bool ConfigBearerProfile();
	bool queryGprsContext();
	bool openGprsContext();
	bool closeGprsContext();
	bool setFtpParameters(void);

	GsmParser *ftpGsmParser;
};
