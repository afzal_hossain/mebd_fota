#pragma once


#include "mbed.h"
#include "GsmParser.h"

#define GSM_RX_PIN 			PA_10
#define GSM_TX_PIN 			PA_9
#define GSM_POWERKEY_PIN	PA_11
#define GSM_STATUS_PIN		PA_11
#define GSM_BAUD_RATE		9600U


class GsmModem : public GsmParser{


public:

	virtual ~GsmModem(void);
	void powerOffGsmModem();
	void resetGsmModem();
	void init();

	bool checkIfFileExistsInGsmModuleFlash(const char * fileName);
	int  getFileSize(const char * filePath,const char *fileName);
	const char * downloadFilePacketFromGsmModuleFlash(char * fileName,int packetSize, int position);


	static GsmModem *getInstance();

private:

	GsmModem(void);
	static GsmModem *instance;

	void powerOnGsmModem();

};
