#include "GsmParser.h"
#include "CSTimer.h"

#include <stdlib.h>
#include <string.h>


#define debug_print

#ifdef debug_print
#include <Utils/Mdebug.h>
Mdebug mDebug;
#endif


GsmParser :: GsmParser(PinName tx, PinName rx,int BaudRate):SerialUtils(tx, rx,BaudRate,GSM_RX_BUFFER_SIZE,GSM_TX_BUFFER_SIZE)
{

}
GsmParser :: ~GsmParser(void){

}
void GsmParser::sendCommand(const char * AtCommandBuffer){

	sendStringToSerial(AtCommandBuffer);

}
char * GsmParser::receiveResponse(int len,char * receiveBuffer){

	return "0";

}

bool GsmParser::varifyCommandResponse(const char * expectedResponse,int responseLength,int timeOut){

#ifdef debug_print
	//mDebug.print_log(gsmRxBuffer.get());
#endif

	CSTimer::myCstimerTimeOut = timeOut;
	while(1){
		if(CSTimer::myCstimerTimeOut <= 0 || gsmRxBuffer.numberOfElementsInBuffer() >= responseLength){
			if(strstr(gsmRxBuffer.get(),expectedResponse)){

#ifdef debug_print
				mDebug.print_log("success\n");
#endif
				return true;
			}
			break;
		}
	}
	return false;
}
int GsmParser::initializeGsmModule(void){

    static struct {

          const char* ATcommand;       const char* ATresponse;     int responseLength;

    } atCmdToInitGsm[] = {
        { "AT\r\n",                   	"OK",            6      },
        { "ATE0\r\n",  					"OK",     		 6	    },
        { "AT+CREG=0\r\n" ,             "OK",     		 6		},
        { "AT+CGREG=0\r\n",             "OK",     	     6		},
		{ "AT+CREG?\r\n",               "OK",     		 25		},
		{ "AT&W\r\n",                   "OK",     	     6		},
		{ "AT+CSQ\r\n",                 "OK",     	     25		},

    };

    for(int i=0; i<7; i++){
    	sendCommand(atCmdToInitGsm[i].ATcommand);
    	//wait_ms(100);
    	bool responseStatus = varifyCommandResponse(atCmdToInitGsm[i].ATresponse,atCmdToInitGsm[i].responseLength,10);

    	if(!responseStatus){
    		i--;
#ifdef debug_print
    		mDebug.print_log("\n error from gsm module \r\n");
#endif
    	}
    }
}
bool GsmParser::checkGsmNetwork(){
	return true;
}
bool GsmParser::checkGprsStatus(){
	return true;
}
bool GsmParser::checkSignalStrength(){
	return true;
}
bool GsmParser::checkIfMessageExists(char index){
	return true;
}


