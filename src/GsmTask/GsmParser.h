#pragma once

#include "mbed.h"
#include "SerialUtils.h"

//#include <stdint.h>



#define GSM_RX_BUFFER_SIZE 125
#define GSM_TX_BUFFER_SIZE 125


class GsmParser : public SerialUtils{

public:
	GsmParser(PinName tx, PinName rx,int BaudRate);
	virtual ~GsmParser(void);


	void sendCommand(const char * AtCommandBuffer);
	char * receiveResponse(int len,char * receiveBuffer);
	bool varifyCommandResponse(const char * expectedResponse,int responseLength,int timeOut);

	virtual bool checkIfFileExistsInGsmModuleFlash(const char * fileName) = 0;
	virtual int  getFileSize(const char * filePath,const char *fileName) = 0;
	virtual const char *downloadFilePacketFromGsmModuleFlash(char * fileName,int packetSize, int position)=0;

protected:

	int initializeGsmModule(void);

private:

	bool checkGsmNetwork();
	bool checkGprsStatus();
	bool checkSignalStrength();
	bool checkIfMessageExists(char index);

};
