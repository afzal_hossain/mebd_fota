#include "Ftp.h"


#define debug_print
#ifdef debug_print
#include <Utils/Mdebug.h>
Mdebug Debug;
#endif



Ftp::Ftp(GsmParser &parser) : ftpGsmParser(&parser)
{

}

Ftp::~Ftp()
{

}

bool Ftp::init(void){

	bool ftpOp = true;
	ftpOp = ConfigBearerProfile();
	if(!ftpOp){

#ifdef debug_print
    	Debug.print_log("FTP_INIT_LOG::error to initialize FTP:::");
#endif
		return false;
	}
    return true;

}
bool Ftp::ConfigBearerProfile(){

	static struct {
	          const char* ATcommand;       const char* ATresponse;     int responseLength;
	}aCmdToConfigBearerP[] = {
			{ "AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r\n",      "OK",       100 },
	        { "AT+SAPBR=3,1,\"APN\",\"INTERNET\"\r\n",  	"OK",     	100	}
	};
    for(int i=0; i<=1; i++){

    	ftpGsmParser -> sendCommand(aCmdToConfigBearerP[i].ATcommand);
    	bool responseStatus = ftpGsmParser ->varifyCommandResponse(aCmdToConfigBearerP[i].ATresponse,aCmdToConfigBearerP[i].responseLength,10);

    	if(!responseStatus){
    			return false;
    	}
    	wait_ms(100);

   }
   return true;
}

bool Ftp::queryGprsContext(){

	static struct {
	          const char* ATcommand;       const char* ATresponse;     int responseLength;
	}aCmdToQueryGprsCntx = { "AT+SAPBR=2,1\r\n",     "OK",    100 };

	ftpGsmParser -> sendCommand(aCmdToQueryGprsCntx.ATcommand);
	bool responseStatus = ftpGsmParser ->varifyCommandResponse(aCmdToQueryGprsCntx.ATresponse,aCmdToQueryGprsCntx.responseLength,10);
	if(!responseStatus){
		//will be implemented  further.............
	}
    return true;

}
bool Ftp::openGprsContext(){

	static struct {
	          const char* ATcommand;      const char* ATresponse;     int responseLength;
	}atCmdToOpenGprsCentext = { "AT+SAPBR=1,1\r\n",      "OK",        100 };

	ftpGsmParser -> sendCommand(atCmdToOpenGprsCentext.ATcommand);
	bool responseStatus = ftpGsmParser ->varifyCommandResponse(atCmdToOpenGprsCentext.ATresponse,atCmdToOpenGprsCentext.responseLength,50);

	if(!responseStatus){
		//will be implemented in future .....................
	}
    return true;
}

bool Ftp::closeGprsContext(){

	static struct {
	          const char* ATcommand;      const char* ATresponse;     int responseLength;
	}atCmdToCloseGprsContext = { "AT+SAPBR=0,1\r\n",      "OK",      100 };

	ftpGsmParser -> sendCommand(atCmdToCloseGprsContext.ATcommand);
	bool responseStatus = ftpGsmParser ->varifyCommandResponse(atCmdToCloseGprsContext.ATresponse,atCmdToCloseGprsContext.responseLength,30);

#ifdef debug_print
    	Debug.print_log(atCmdToCloseGprsContext.ATcommand);
#endif

    return true;
}

bool Ftp::setFtpParameters(void){

	static struct {
	     const char* ATcommand;       const char* ATresponse;     int responseLength;
	} stCmdToSetFtpParameters[] = {

		{ "AT+FTPSERV=\"pilabsbd.com\"\r\n",                	"OK",     	     100		},
		{ "AT+FTPPORT=21\r\n",                 					"OK",     	     100		},
		{ "AT+FTPUN=\"fota@pilabsbd.com\"\r\n",             	"OK",     	     100		},
		{ "AT+FTPPW=\"T8f9mX92c2\"\r\n",                 		"OK",     	     100		},
		{ "AT+FTPGETNAME=\"bld_app_no_mbed.bin\"\r\n",      	"OK",     	     100		},
		{ "AT+FTPGETPATH=\"/\"\r\n",                 			"OK",     	     100		}

	 };
    for(int i=0; i<=5; i++){

    	ftpGsmParser ->sendCommand(stCmdToSetFtpParameters[i].ATcommand);
    	bool responseStatus = ftpGsmParser ->varifyCommandResponse(stCmdToSetFtpParameters[i].ATresponse,stCmdToSetFtpParameters[i].responseLength,50);
    	wait_ms(100);
    }
    return true;

}

bool Ftp::downloadAndSaveFileToGsmModuleFlash(const char * fileName){

	bool ftpStatus = Ftp::init();
	if(!ftpStatus) {

#ifdef debug_print
    	Debug.print_log("Error to download file from server.....");
#endif
		return false;
	}

	ftpStatus &= queryGprsContext();
	ftpStatus &= openGprsContext(); //if queryGprsContext indicates that gprsContext is open then this command is not needed ... @attile.babu.khan
	ftpStatus &= queryGprsContext(); //confirm gprsContext is open
	ftpStatus &= setFtpParameters();



	static struct {
	     const char* ATcommand;       const char* ATresponse;     int responseLength;

	} atCmdToDownLoadAndSaveToFlash = { "AT+FTPGETTOFS =0,\"bld_app_no_mbed.bin\"\r\n",       "OK",     	     100		};
	ftpGsmParser ->sendCommand(atCmdToDownLoadAndSaveToFlash.ATcommand);
	wait(59.0);
//	{
//		// need to check if file is downloaded or not ... will be implemented further .
//	}
	closeGprsContext();

	return true;
}
