#include "GsmModem.h"
#include <stdlib.h>
#include <string.h>



#define debug_print
#ifdef debug_print
#include "Utils/Mdebug.h"
Mdebug cebug;
#endif




GsmModem::GsmModem(void):GsmParser(GSM_TX_PIN,GSM_RX_PIN,GSM_BAUD_RATE)
{


}
GsmModem::~GsmModem(void){
	powerOffGsmModem();
}

GsmModem * GsmModem::instance = NULL  ;

GsmModem * GsmModem::getInstance(){
	if (!instance){
		 instance = new GsmModem;
	}
	return instance;
}


void GsmModem::init(){
	powerOnGsmModem();
	initializeGsmModule();
}

void GsmModem::powerOnGsmModem(){

	DigitalOut pin(GSM_POWERKEY_PIN, 1);

   // pin = 0; ::wait_us(50);
   // pin = 1; ::wait_ms(1500);
   // pin = 0; ::wait_us(50);


}
void GsmModem::powerOffGsmModem(){

}
void GsmModem::resetGsmModem(){


}



//**************file downFrom modem flash *********************//


bool GsmModem::checkIfFileExistsInGsmModuleFlash(const char * fileName){

	static struct {
		const char* ATcommand;       const char* ATresponse;     int responseLength;
	} checkFlashDrive[] = {
		{ "AT+FSDRIVE=?\r\n",                "OK",     	     100},
		{ "AT+FSDRIVE=0\r\n",                 "OK",     	 100},
		{ "AT+FSLS=C:\\User\\FTP\\\r\n",      "OK",     	 100}
	};
	for(int i=0; i<=2; i++){
		sendCommand(checkFlashDrive[i].ATcommand);
		wait_ms(2000);
	}
}

int GsmModem::getFileSize(const char * filePath,const char *fileName){

	char arrayOfFileSizeString[15];
	char *prt_of_fileSizeString =  arrayOfFileSizeString;

	static struct {
		   const char* ATcommand;       const char* ATresponse;     int responseLength;
	} atCmdToGetFileSz = { "AT+FSFLSIZE=C:\\User\\FTP\\bld_app_no_mbed.bin\r\n",    "OK",   25};
	sendCommand(atCmdToGetFileSz.ATcommand);
	bool responseOk = varifyCommandResponse(atCmdToGetFileSz.ATresponse,atCmdToGetFileSz.responseLength,20);
	if(!responseOk){
		return 0;
	}

	const char * atResponseBuffer = gsmRxBuffer.get();
	while(*atResponseBuffer){
		if(*atResponseBuffer++ == ' '){
			while(1){
				if(*atResponseBuffer == '\r' || *atResponseBuffer == '\n'){
					break;
				}
				*prt_of_fileSizeString++ = *atResponseBuffer++;
			}
			*prt_of_fileSizeString = '\0';
			break;
		}
	}
	return  atoi(arrayOfFileSizeString);
}

const char * GsmModem::downloadFilePacketFromGsmModuleFlash(char * fileName,int packetSize, int position){

	//char at_fileread[] = "AT+FSREAD=C:\\User\\FTP\\bld_app_no_mbed.bin,1,255,1\r\n";
	char tempBuffer[10];
	char atCmdToDownLoadBytePacket[150];

	strcpy(atCmdToDownLoadBytePacket,"AT+FSREAD=C:\\User\\FTP\\");
	strcat(atCmdToDownLoadBytePacket,fileName);   //strcat(at_command_to_download_packet,"bld_app_no_mbed.bin");
	strcat(atCmdToDownLoadBytePacket,",");
	strcat(atCmdToDownLoadBytePacket,"1");
	strcat(atCmdToDownLoadBytePacket,",");
	itoa(packetSize,tempBuffer,10);
	strcat(atCmdToDownLoadBytePacket,tempBuffer);
	strcat(atCmdToDownLoadBytePacket,",");
	itoa(position,tempBuffer,10);
	strcat(atCmdToDownLoadBytePacket,tempBuffer);
	strcat(atCmdToDownLoadBytePacket,"\r\n");

	sendCommand(atCmdToDownLoadBytePacket);
	bool responseOk = varifyCommandResponse("OK",280,50);
//	if(!responseOk){
//		return 0;
//	}

//#ifdef debug_print
//	cebug.print_log("\nGsmModem::received packet :: \n ");
//	cebug.print_log_with_nul(gsmRxBuffer.get(),280);
//#endif


	return gsmRxBuffer.get();
}

